/*...................................................................
......................................................................
......................................................................
....................Tecnologico de Costa Rica.........................
......................................................................
..................Principio De Sistemas Operativos....................
......................................................................
......................................................................
...........................Estudiantes................................
...................  Lorenzo Mendez Rodriguez.........................
...................  Alexis Torres Agüero ............................
......................................................................
............................Profesor..................................
.....................Kevin Moraga.....................................
......................................................................
......................................................................
........................1-Semestre-2019...............................
......................................................................*/

#include <stdio.h> /* perror, fdopen, fgets */
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <curl/curl.h>

void descargar_recurso(char *url ,char *name);
void leearchivo();
char* obtener_nombre(char *url);


/*
Entradas: argc numero de parametros recibidos, argv paramtros recibidos
Salidas: Archivo solicitado por el cliente en el url
Restricciones: El primer parametro debe ser de tipo numero y el segundo de tipo char arreglo
*/
int main(int argc, char **argv){
	char *fireFox = "firefox file://";
	char localPath[100];

	getcwd(localPath,100);



	if (argc != 3  || (strcmp(argv[1],"-u")!=0)){
      printf("usage: %s -u <url-de-recurso-a-obtener> \n", argv[0]);
      exit(-1);
    }

  	
	char *url = argv[2];
	char *name = obtener_nombre(url);

	char str[100];
	char unlockFile[50];
	char executeFile[50];

	char *unlock = "chmod 777 ";
	char *execute = "./";
	char *slash = "/";
	char parserName[15];
	strcpy(str,fireFox);//ejecuta comando de terminal xdg-open para abrir un archivo
	strcpy(parserName,name);

	strcat(str,localPath);
	strcat(str,slash);
	//strcat(str,name);

	strcpy(unlockFile,unlock);
	strcat(unlockFile,name);




	
	char *isPoint =  strchr(name, '.');

	descargar_recurso(url,name);

	if (isPoint == NULL){
		system(unlockFile);

		strcpy(executeFile,execute);
		strcat(executeFile,name);
	    system(executeFile);
	
		
	}else{
		strcat(str,name);
		system(str);
	}
	
}

/*
Entradas: Url ingresado por el usuario
Salidas: El nombre del archivo solicitado al servidor por el usuario
Restricciones: El parametro de entrada debe ser de tipo char
*/
char* obtener_nombre(char *url){
	int ultimo_slash = 0;
	for(int i= 0; i<strlen(url); i++){
		if(url[i]=='/'){
			ultimo_slash=i+1;
		}
	}
	return &url[ultimo_slash];
}

/*
Entradas: Url del recurso a descargar y nombre del recurso este para guardarlo en la carpeta
del cliente con el mismo nombre
Salidas: Descarga y escribe lo que descargo en el archivo que crea con el nombre recibido por parametro
Restricciones: Ambos datos de entrada debe ser de tipo char.
*/  
void descargar_recurso(char *url,char *name){
	char buffer[12040] = {0};

	size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
		size_t written = fwrite(ptr, size, nmemb, stream);
		return written;
	}

	CURL *curl;
	CURLcode res;
	FILE *fp;

	curl = curl_easy_init();
	fp = fopen(name,"wb");//donde guardare el archivo descargado

	//link de descarga para los recursos
	curl_easy_setopt(curl, CURLOPT_URL,url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data); // la informacion del archivo
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);//la escrimos aca
	

	curl_easy_perform(curl);
	curl_easy_cleanup(curl);


	fclose(fp);//cerramos el archivo	
}