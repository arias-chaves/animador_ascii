/*;...................................................................
......................................................................
......................................................................
....................Tecnologico de Costa Rica.........................
......................................................................
..................Principio De Sistemas Operativos....................
......................................................................
......................................................................
...........................Estudiantes................................
...................  Lorenzo Mendez Rodriguez.........................
...................  Alexis Torres Agüero ............................
......................................................................
............................Profesor..................................
.....................Kevin Moraga.....................................
......................................................................
......................................................................
........................1-Semestre-2019...............................
......................................................................*/
#include "utils.h"
#include "control_process.h"

int server; // Descriptor de archivo, para aceptar conexiones
int currentRequest;
int max_connections, port;



// Id de la conexion del asociada al cliente
// Puntero a estructura sockaddr_in que contiene una dirección de Internet
typedef struct{
	int acceptfd;
	struct sockaddr_in clientAddrs;
} clientInformation;


/*Crea un nuevo socket*/
void createSocket(){
	//Socket de dominio de Internet.
	server = socket(AF_INET, SOCK_STREAM, 0);
	if (server == -1) //Error
		printf("Error mientras de creaba el socket\n");
	//printf("Socket creado correctamnete!!\n");
}

/*
Entradas: Puerto de conexion.
Salidas: Crea una estructura que contiene una dirección de Internet.
Restricciones: El parametro de entrada debe ser de tipo entero.
*/
void bindSocket(int port_server){
	struct sockaddr_in serverAddrs;
	memset(&serverAddrs, 0, sizeof(serverAddrs));
	serverAddrs.sin_family = AF_INET;
	serverAddrs.sin_port = htons(port_server);
	serverAddrs.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(server, (struct sockaddr *) &serverAddrs, sizeof(serverAddrs)) < 0)
		printf("bind() fallo!!\n");
	//printf("bind() exito!!\n");
}

/*
Entradas: Puerto de conexion y cantidad maxima de conexiones en cola que soportara el servidor.
Salidas: Llama a las funciones createSocket() y bindSocket(port_server). Se mantiene escuchando conexiones entrantes
Restricciones: Ambos parametros de entrada deben ser de tipo entero positivo.
*/
void createServer(int port_server,int connections){
	port = port_server;
	createSocket();
	bindSocket(port_server);
	max_connections = connections;
	//segundo parametro numero de conexiones en cola
	if (listen(server, connections) < 0)
		printf("listen() fallo!!\n");
	//printf("listen() exito!!\n");
}

/*
Entradas: Recibe como entrada la estructura que contendra la informacion relevante del cliente 
Salidas: Crea un nuevo descriptor de archivo, ademas agrega el id de conexion y la estructura asociada
a la informacion del cliente en estrutura clientinfo
Restricciones: El parametro de entrada debe ser de tipo struct
*/
void acceptConnection(clientInformation *client){
	int acceptfd, clientLength;;
	clientLength = sizeof(client->clientAddrs);
	
	//Bloqueado, esperando conexion.
	acceptfd = accept(server, (struct sockaddr *) &(client->clientAddrs), &clientLength);
	if (acceptfd == -1)
		printf("accept() Se ha presentado un error al intentar aceptar una nueva conexion entrante.\n");
	//printf("accept() La conexion entrante ha sido aceptada\n");
	//currentRequest++;
	
	client->acceptfd = acceptfd;
}

/*
Entradas: ID de descriptor de archivo
Salidas: Atiende la solicitud GET y la procesa deacuerdo a los parametros recibidos, una vez procesada
envia respuesta al cliente.
Restricciones: El descriptor de archivo debe existir y ser de tipo numero entero positivo.
*/
void attendOutputGetRequest(int socketfd) {
	int status = 0;
	char buf[BUF_SIZE];
	char *method;
	char *filename;
	long fsize=0;
	//currentRequest = procesos_disponibles;
	//Recibe solicitud
	recv(socketfd, buf, BUF_SIZE, 0);
	//atiende las solictudes y responde con el header adecuado para cada protocolo
	

	
	if (strcmp(buf,"exit")==0){
		//closeChildProcess(max_connections);
		//exit(0);
	}

	}else{
		
		filename = strtok(NULL, " ");
		

		FILE *file = fopen(filename, "rb");

		if(file == NULL){
            printf("Error al abrir el archivo");
	    	exit(0);
		} 

		fseek(file, 0L, 2);
        fsize=ftell(file);
        long int doter=(long)fsize/100;
        printf("Tamano: %lu bytes\n",fsize);

		fclose(file);

		//**********************************************

		file = fopen(filename, "rb");

		if(file == NULL){
            printf("Error al abrir el archivo");
	    	exit(0);
		} 

		//Leer informacion del archivo y enviarla por bloques
        while(1){
            //Envio 256 bytes
            unsigned char buff[1024]={0};
            int nread = fread(buff,1,1024,file);
            if(nread > 0){
                write(socketfd, buff, nread);
			}

			if (nread < 1024){
				if (feof(file)){
                    printf("Final del archivo\n");
		    		printf("Transferecia del archivo completada\n\n");
		    		currentRequest--;
		    		//printf("%d\n", currentRequest);
				}
                if (ferror(file))
                    printf("Error al leer\n");
					break;
			}
		}
	}

}