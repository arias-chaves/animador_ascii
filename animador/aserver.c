/*
    Luis Antonio Arias
    Iván Quesada
    Alexis Torres
    Progra de Moraga, DMA
*/
 
#include<stdio.h>
#include<string.h>    
#include<stdlib.h>    
#include<sys/socket.h>
#include<arpa/inet.h> 
#include<unistd.h>    
#include<pthread.h> 
#include<time.h>


//int max_x = 20;
//int max_y = 40;
//int max_string = 150;

void *connection_handler(void *);
struct figuraInfo //Struct para guardar las posiciones de cada una de las figuras
{ 
   int pantallaActual, xActual,yActual, pantallaFinal, xFinal, yFinal; 
}; 


int bandera = 0;
int banderaFinish = 0;

//***********************************  //Arreglo que strings que contiene el nombre de los archivos a animar
char arreglo_figuras[40][150];
int len_figuras = 0;
//***********************************

struct figuraInfo arreglo_figuras_mov[40]; // Struct que contiene caracteristicas de los archivos,

int matrices_auxiliares[4][20][40]; //Matrices que simulan las pantallas

int pantallaIndex = 0;
pthread_mutex_t lock;





char* cargarAnimacion(){ // Esta funcion es como un lexer patito que llena las estructuras globales
	//Carga el archivo aniconfig.fk con las instrucciones a ejecutar
	char temp[150];
	int i = 0;
	int j = 0;
    FILE *f = fopen("aniconfig.fk", "r");
	int letra = getc(f);
	




	while (letra != EOF){
		if(letra == '#'){
			while(letra != 10 && letra != EOF){
				letra = getc(f);
			}
			letra = getc(f);
		}else{
			if(letra == 10){
				while(letra == 10 && letra != EOF){
				letra = getc(f);
				}		
			}else{
				while(letra != 32){
					temp[j] = letra;
					j++;
					letra = getc(f);
				}
				len_figuras++;
				temp[j]=0;
				strncpy(arreglo_figuras[i], temp, j);	
				arreglo_figuras_mov[i].pantallaActual = (int)getc(f) - 49;
				getc(f);


				arreglo_figuras_mov[i].xActual = (int)getc(f) - 48;
				letra = getc(f);
				if(letra != 32){// es caracter blanco
					arreglo_figuras_mov[i].xActual = (arreglo_figuras_mov[i].xActual * 10) + (int)(letra - 48);
					getc(f); // se come el espacio
				}
				
				arreglo_figuras_mov[i].yActual = (int)getc(f) - 48;
				letra = getc(f);
				if(letra != 32){// es caracter blanco
					arreglo_figuras_mov[i].yActual = (arreglo_figuras_mov[i].yActual * 10) + (int)(letra - 48);
					getc(f); // se come el espacio
				}
				
				arreglo_figuras_mov[i].pantallaFinal = (int)getc(f) - 49;
				getc(f);
				

				arreglo_figuras_mov[i].xFinal = (int)getc(f) - 48;
				letra = getc(f);
				if(letra != 32){// es caracter blanco
					arreglo_figuras_mov[i].xFinal = (arreglo_figuras_mov[i].xFinal * 10) + (int)(letra - 48);
					getc(f); // se come el espacio
				}

				arreglo_figuras_mov[i].yFinal = (int)getc(f) - 48;
				letra = getc(f);
				if(letra != 32 && letra != 10){// es caracter blanco o cambio de linea
					arreglo_figuras_mov[i].yFinal = (arreglo_figuras_mov[i].yFinal * 10) + (int)(letra - 48);
				}
				j=0;	
				i++;

				while(letra !=10 && letra != EOF){
					letra = getc(f);
				}
				
			}
				
		}		
	}
	fclose(f);	
	return 0;
	
}

char * leerFigura(char* imagen){ //Le entra el nombre del archivo, lo busca y lo carga en un string
    FILE *f = fopen(imagen, "rb");
    fseek(f, 0, SEEK_END);
    long largo = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *texto = malloc(largo + 1);
    fread(texto, 1, largo, f);
    fclose(f);
    texto[largo] = 0;
    return (texto);
}


void prepararFigura(char * img,int id_figura){//Ingresa en las matrices de simulacion las imagenes
	//img es el string en memoria donde esta la imagen cargada
	//id_figura es la posicion de la figura en el arreglo de las figuras


	int temp_x = arreglo_figuras_mov[id_figura].xActual;
	int temp_y = arreglo_figuras_mov[id_figura].yActual;
	int temp_pantalla = arreglo_figuras_mov[id_figura].pantallaActual;

	for(int i=0; i<strlen(img); i++){//inserta los caracteres en las matrices, antes de esto hay que ver el crash
		if(img[i] == 10){
			temp_y = arreglo_figuras_mov[id_figura].yActual;
			temp_x++;
			if(temp_x == 20){
				temp_pantalla++;
				temp_x=0;
			}
		}else{
			matrices_auxiliares[temp_pantalla][temp_x][temp_y] = img[i];
			temp_y++;
			if(temp_y == 40){
				temp_pantalla++;
				temp_y=0;
			}
		}
	}
}

void sigFig(int indice_figura){
	int indice_numero = strlen(arreglo_figuras[indice_figura])-1;
	int temp = ((int) arreglo_figuras[indice_figura][indice_numero]) - 48;
	temp++;
	arreglo_figuras[indice_figura][indice_numero] = temp + 48;
	FILE * ftemp = fopen(arreglo_figuras[indice_figura],"r");
	if(ftemp == NULL){
		arreglo_figuras[indice_figura][indice_numero] = '1';
	}
	fclose(ftemp);
}

int actualizarFigura(int indice_figura){

	if(arreglo_figuras_mov->pantallaActual < arreglo_figuras_mov->pantallaFinal){
		if(arreglo_figuras_mov->xActual = 20){
			arreglo_figuras_mov->pantallaActual++;
		}else{
			arreglo_figuras_mov->xActual++;
		}
	}else{
		if(arreglo_figuras_mov->xActual < arreglo_figuras_mov->xFinal){
			arreglo_figuras_mov->xActual++;
		}
	}
	if(arreglo_figuras_mov->yActual < arreglo_figuras_mov->yFinal){
		arreglo_figuras_mov->yActual++;
	}
	if(arreglo_figuras_mov->yActual == arreglo_figuras_mov->yFinal && arreglo_figuras_mov->xActual == arreglo_figuras_mov->xFinal){
		banderaFinish = 1;
		return 0;
	}
	sigFig(indice_figura);

}

void renderizar(){ //Toma las figuras con sus respectivas posiciones y las acomoda a todas en las matrices de simulacion
	char *imagen = malloc(100);
	char *img_tmp;
	char ar_imagen[100];
	for(int i=0; i < len_figuras; i++){
		strncpy(imagen,arreglo_figuras[i],strlen(arreglo_figuras[i]));
		imagen[strlen(arreglo_figuras[i])]=0;
		img_tmp = leerFigura(imagen);
		prepararFigura(img_tmp,i);
		}
}


char * aplanar(int id_pantalla){ // Aplana la matriz a un string, el id simplemente es la pantalla que se quiere aplanar y este retorna la direccion donde esta el string aplanado
	int j = 0;
	int indice_result = 0;
	int char_temp;
	char * result = malloc((20*40) + 25);
	for(int i=0; i<20;i++){
		for(j;j<40;j++){
			char_temp = matrices_auxiliares[id_pantalla][i][j];
			if(char_temp == 0){
				result[indice_result] = '`';		
			}else{
				result[indice_result] = char_temp;
			}	



			indice_result++;
		}
		j=0;
		result[indice_result] = 10;
		indice_result++;
	}
	indice_result--;
	result[indice_result]=0;
	return result;
}















 
int main(int argc , char *argv[])
{
	cargarAnimacion();
	renderizar();
    int socket_desc , client_sock , c;
    struct sockaddr_in server , client;
     
    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Error, no se pudo crear el servidor");
    }
    puts("servidor up");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 8888 );
     
    //Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("Fallo el enlaze. Error");
        return 1;
    }
    puts("Enlaze con exito");
     
    //Listen
    listen(socket_desc , 3);
     
    
    //Accept and incoming connection
    puts("Esperando monitores");
    c = sizeof(struct sockaddr_in);
	pthread_t thread_id[5];
	


    while( client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c) )
    {
        puts("Conexion Aceptada");
         
        if( pthread_create( &(thread_id[0]) , NULL ,  connection_handler , (void*) &client_sock) < 0)
        {
            perror("could not create thread");
            return 1;
        }
        puts("Conexion Exitosa");
    }
     
    if (client_sock < 0)
    {
        perror("Error");
        return 1;
    }
     
    return 0;
}
 
/*
 * This will handle connection for each client
 * */
void *connection_handler(void *socket_desc)
{
    int sock = *(int*)socket_desc;    
    char *message = aplanar(0);
    write(sock , message , strlen(message));
     
} 
