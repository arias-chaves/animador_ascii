#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include <netinet/in.h>






#define COLOR_ROJO     "\x1b[31m"
#define COLOR_AMARILLA  "\x1b[33m"
#define COLOR_AZUL    "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN    "\x1b[36m"
#define COLOR_RESET   "\x1b[0m"



int main()
{


//Declarando socket

int cliente_socket;
cliente_socket = socket(AF_INET, SOCK_STREAM, 0 ); //AF_INET = socket internet //SOCK_STREAM = Protocolo TCP // 0 ?

//Direccion
struct sockaddr_in direccion_cliente; 
direccion_cliente.sin_family = AF_INET; // familia donde se va conectar
direccion_cliente.sin_port = htons(8888); // puerto donde pasara la info, htons estructura el puerto para solo pasarle el numero
direccion_cliente.sin_addr.s_addr = INADDR_ANY; // donde se encuentra el servidor(local, igual lo estrucuta, puerto 0000)


int estado_conexion = connect(cliente_socket, (struct sockaddr * ) &direccion_cliente, sizeof(direccion_cliente));

if (estado_conexion == -1){
	printf("Error \n");
	close(1);
}else{
	printf("Conexion establecida \n");
}

	int len_mensaje;
	char mensaje[3000];
	while(1){
		bzero(mensaje,strlen(mensaje));		
		len_mensaje = recv(cliente_socket, mensaje, 3000, 0);
		mensaje[len_mensaje]='\0';
		if( len_mensaje < 0){
			printf("Error al recibir datos \b");
		}else{
			printf(COLOR_AZUL "\n%s" COLOR_RESET"\n",mensaje );
			
		}		
	return 0;
	}
}
