#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>                  /*  for sleep()  */
#include <curses.h>
#include <string.h>

//int mvwaddch(WINDOW *win, int y, int x, const chtype ch);

char arrelgo[200][200];


char * leerFigura(){
    FILE *f = fopen("cuadrado", "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

    char *texto = malloc(fsize + 1);
    fread(texto, 1, fsize, f);
    fclose(f);

    texto[fsize] = 0;

    return (texto);
}

//Entradas
// x,y = coordenadas de la esquina superior izquierda para dibujar la figura
// figura = String que contiene la figura
void pintarCuadrado(WINDOW * ventana,int x, int y, char * figura, int rotar){
    int posicion_x_inicial = x;
    int i = 0;
    int j = 0;

    while(*figura != 0){
        if(*figura == 10){
            y++;
            x = posicion_x_inicial;
            figura++;
        }else{
            mvwaddch(ventana,y, x, *figura);
            x++;
            figura++;
        }
    }
}




int main(void) {
    char* figura = leerFigura();
    WINDOW * mainwin;
    mainwin = initscr();
    raw();
    
    //cbreak();

    int i = 0;
    int x = 0;
    int y = 0;
    int rotar = 0;

    for(i;i<15;i++){
        if(rotar!=0){
            pintarCuadrado(mainwin,x,y,figura,rotar);
            rotar=0;    
        }else{
            pintarCuadrado(mainwin,x,y,figura,rotar);
            rotar=1;    
        }
        
        refresh();
        sleep(1);
        clear();
        x++;
        
    }
    
    
    delwin(mainwin);
    endwin();
    refresh();

    return EXIT_SUCCESS;
}