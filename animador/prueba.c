#include<stdio.h>
#include<string.h>    
#include<stdlib.h>    




struct figuraInfo //Struct para guardar las posiciones de cada una de las figuras
{ 
   int pantallaActual, xActual,yActual, pantallaFinal, xFinal, yFinal; 
}; 
//***********************************  //Arreglo que strings que contiene el nombre de los archivos a animar
char arreglo_figuras[40][150];
int len_figuras = 0;
//***********************************

struct figuraInfo arreglo_figuras_mov[40];// Struct que contiene caracteristicas de los archivos,
int matrices_auxiliares[4][40][80];//Matrices que simulan las pantallas





char* cargarAnimacion(){ // Esta funcion es como un lexer patito que llena las estructuras globales
	//Carga el archivo aniconfig.fk con las instrucciones a ejecutar
	char temp[150];
	int i = 0;
	int j = 0;
    FILE *f = fopen("aniconfig.fk", "r");
	int letra = getc(f);
	




	while (letra != EOF){
		if(letra == '#'){
			while(letra != 10 && letra != EOF){
				letra = getc(f);
			}
			letra = getc(f);
		}else{
			if(letra == 10){
				while(letra == 10 && letra != EOF){
				letra = getc(f);
				}		
			}else{
				while(letra != 32){
					temp[j] = letra;
					j++;
					letra = getc(f);
				}
				len_figuras++;
				temp[j]=0;
				strncpy(arreglo_figuras[i], temp, j);	
				arreglo_figuras_mov[i].pantallaActual = (int)getc(f) - 49;
				getc(f);


				arreglo_figuras_mov[i].xActual = (int)getc(f) - 48;
				letra = getc(f);
				if(letra != 32){// es caracter blanco
					arreglo_figuras_mov[i].xActual = (arreglo_figuras_mov[i].xActual * 10) + (int)(letra - 48);
					getc(f); // se come el espacio
				}
				
				arreglo_figuras_mov[i].yActual = (int)getc(f) - 48;
				letra = getc(f);
				if(letra != 32){// es caracter blanco
					arreglo_figuras_mov[i].yActual = (arreglo_figuras_mov[i].yActual * 10) + (int)(letra - 48);
					getc(f); // se come el espacio
				}
				
				arreglo_figuras_mov[i].pantallaFinal = (int)getc(f) - 49;
				getc(f);
				

				arreglo_figuras_mov[i].xFinal = (int)getc(f) - 48;
				letra = getc(f);
				if(letra != 32){// es caracter blanco
					arreglo_figuras_mov[i].xFinal = (arreglo_figuras_mov[i].xFinal * 10) + (int)(letra - 48);
					getc(f); // se come el espacio
				}

				arreglo_figuras_mov[i].yFinal = (int)getc(f) - 48;
				letra = getc(f);
				if(letra != 32 && letra != 10){// es caracter blanco o cambio de linea
					arreglo_figuras_mov[i].yFinal = (arreglo_figuras_mov[i].yFinal * 10) + (int)(letra - 48);
				}
				j=0;	
				i++;

				while(letra !=10 && letra != EOF){
					letra = getc(f);
				}
				
			}
				
		}		
	}
	fclose(f);	
	return 0;
	
}

char * leerFigura(char * fig){
    FILE *f = fopen(fig, "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

    char *texto = malloc(fsize + 1);
    fread(texto, 1, fsize, f);
    fclose(f);

    texto[fsize] = 0;

    return (texto);
}

void prepararFigura(char * img,int id_figura){


	int temp_x = arreglo_figuras_mov[id_figura].xActual;
	int temp_y = arreglo_figuras_mov[id_figura].yActual;
	int temp_pantalla = arreglo_figuras_mov[id_figura].pantallaActual;

	for(int i=0; i<strlen(img); i++){//inserta los caracteres en las matrices, antes de esto hay que ver el crash
		if(img[i] == 10){
			temp_y = arreglo_figuras_mov[id_figura].yActual;
			temp_x++;
			if(temp_x == 40){
				temp_pantalla++;
				temp_x=0;
			}
		}else{
			matrices_auxiliares[temp_pantalla][temp_x][temp_y] = img[i];
			temp_y++;
			if(temp_y == 80){
				temp_pantalla++;
				temp_y=0;
			}
		}
	}
}

void renderizar(){
	char *imagen = malloc(100);
	char *img_tmp;
	char ar_imagen[100];
	for(int i=0; i < len_figuras; i++){
		strncpy(imagen,arreglo_figuras[i],strlen(arreglo_figuras[i]));
		imagen[strlen(arreglo_figuras[i])]=0;
		img_tmp = leerFigura(imagen);
		prepararFigura(img_tmp,i);
		}
}


char * aplanar(int id_pantalla){
	int j = 0;
	int indice_result = 0;
	int char_temp;
	char * result = malloc((20*30) + 25);
	for(int i=0; i<20;i++){
		for(j;j<30;j++){
			char_temp = matrices_auxiliares[id_pantalla][i][j];
			if(char_temp == 0){
				result[indice_result] = '`';		
			}else{
				result[indice_result] = char_temp;
			}	



			indice_result++;
		}
		j=0;
		result[indice_result] = 10;
		indice_result++;
	}
	indice_result--;
	result[indice_result]=0;
	return result;
}


int main (int argc , char *argv[]){
	FILE* x = fopen("asd","r");
	if(x == NULL){
		printf("%c\n",65);	
	}
	
	return 0;
}

