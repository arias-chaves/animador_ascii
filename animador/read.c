#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(void){

FILE *f = fopen("cuadrado.txt", "rb");
fseek(f, 0, SEEK_END);
long fsize = ftell(f);
fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

char *texto = malloc(fsize + 1);
fread(texto, 1, fsize, f);
fclose(f);

texto[fsize] = 0;

printf("%s",texto);
return(0);
}