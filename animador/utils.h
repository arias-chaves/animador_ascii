 /* File add.h */
 #ifndef UTILS_H
 #define UTILS_H

#include <unistd.h> /* fork, close */
#include <stdlib.h> /* exit */
#include <string.h> /* strlen */
#include <stdio.h> /* perror, fdopen, fgets */
#include <sys/socket.h>
#include <sys/wait.h> /* waitpid */
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/stat.h>
#include <pthread.h>



#define SIZE 20
#define BUF_SIZE 200000


#endif /* UTILS_H */