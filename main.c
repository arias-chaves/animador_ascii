#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <mypthreads.h>

#include <time.h>

int result; //region critica
int cant_threads = 10;

void *sum_with_thread(void *arg){

  mythread_yield();

  pid_t id = __mythread_gettid();
  int a = 1;
  int b = 3;
  result += a + b;
  printf("Se sumó el valor, en el hilo: %d\n", id);
  printf("El valor actual es: %d\n", result);

  mythread_end(NULL);//finaliza el thread
  return NULL;
}

int main(int argc, char const *argv[]) {

  char *status;
  mythread_t threads[cant_threads]; //NTHREADS = 8 maximo de hilos creados

  //Inicializa variables globales
  mythread_init();

  for(int x = 0; x < cant_threads;x++){

    //Parametros(threadID, attr, *funcion, argumentos, prioridad)
    mythread_create(&threads[x], NULL, sum_with_thread, NULL, 1);
  }
  //Cambia del algoritmo de scheduling  parametro(int sched) -> 0 = RR, 1 = lottery, 2 = RT
  mythread_chsched(0);
  //espera por los hilos

  for(int i = 0;i<cant_threads;i++){

      mythread_join(threads[i], (void **)&status);
  }

  //desencola un hilo y lo mata
  mythread_end(NULL);

  return 0;
}
